# Heiko Menzler
# heikogeorg.menzler@stud.uni-goettingen.de
#
# Date: 28.10.2021

from board import SudokuBoard


class Solved(Exception):
    """Exception to be raised to exit out of recursion immidiatly."""


def solve_naive(board: SudokuBoard):
    """Try to solve the sudoku naivly (might return unsolved board)."""
    while not board.is_solved():
        changed = False
        for play_field in board.iter_available():
            possible = play_field.possible_values()
            if len(possible) == 1:
                play_field.value = possible.pop()
                changed = True
        if not changed:
            break


def solve_dfs_step(board: SudokuBoard):
    """."""
    for play_field in board.iter_available():
        possible = play_field.possible_values()

        # if there is a field with no possible entries the branch has failed
        if len(possible) == 0:
            return
        # if there is only one possibility for the field we can set and go next
        if len(possible) == 1:
            play_field.value = possible.pop()
            continue

        for poss in possible:
            play_field.value = poss
            solve_dfs_step(board)
            if board.is_solved():
                raise Solved

        play_field.value = None

    raise Solved


def solve_dfs(board: SudokuBoard):
    try:
        solve_dfs_step(board)
    except Solved:
        pass


def solve_mixed(board: SudokuBoard):
    """Solve the sudoku with a mixed strategy."""
    try:
        while not board.is_solved():
            solve_naive(board)
            solve_dfs_step(board)
    except Solved:
        pass
