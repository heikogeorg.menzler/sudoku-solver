# Heiko Menzler
# heikogeorg.menzler@stud.uni-goettingen.de
#
# Date: 28.10.2021

import itertools
from dataclasses import dataclass, field
from typing import Optional, Tuple, List, Set, Union, Iterable


@dataclass(order=True)
class Field:
    """Simple class representing a field on a sudoko playboard."""
    coordinate: Tuple[int, int]
    collections: List = field(default_factory=list)
    value: Optional[int] = None

    def possible_values(self) -> Set[int]:
        """Return the set difference of possibilities and occupied values."""
        possible_values = {1, 2, 3, 4, 5, 6, 7, 8, 9}
        occupied = map(lambda c: c.values, self.collections)
        return possible_values.difference(*occupied)

    def __setattr__(self, attr, value):
        """Unregister/register 'value' in collections before setting it."""
        if attr != "value":
            super(Field, self).__setattr__(attr, value)
        if attr == "value":
            if hasattr(self, "value") and self.value is not None:
                for coll in self.collections:
                    coll.values.remove(self.value)
            if value is not None:
                for coll in self.collections:
                    coll.values.add(value)
            super(Field, self).__setattr__(attr, value)


@dataclass
class Collection:
    """Generic Collection to link up all fields and their values."""
    values: Set[int] = field(default_factory=set)
    coordinates: List[Tuple[int, int]] = field(default_factory=list)

    def __contains__(self, other: Union[int, Tuple[int, int]]):
        if isinstance(other, int):
            assert 0 < other <= 9, "Values have to range from 1 to 9"
            return other in self.values
        if isinstance(other, tuple):
            assert 0 <= other[0] < 9 and 0 <= other[1] < 9
            return other in self.coordinates

    def is_complete(self) -> bool:
        """Check if the collection is completed."""
        return len(self.values) == 9


class SudokuRow(Collection):
    """Collection in a row of a sudoko playboard."""
    def __init__(self, ridx: int):
        coordinates = list((ridx, cidx) for cidx in range(0, 9))
        super(SudokuRow, self).__init__(coordinates=coordinates)


class SudokuColumn(Collection):
    """Collection in a column of a sudoko playboard."""
    def __init__(self, cidx: int):
        coordinates = list((ridx, cidx) for ridx in range(0, 9))
        super(SudokuColumn, self).__init__(coordinates=coordinates)


class SudokuBox(Collection):
    """Collection in a box of a sudoko playboard."""
    def __init__(self, cidx: int, ridx: int):
        coordinates = list(self.box_coordinates(cidx, ridx))
        super(SudokuBox, self).__init__(coordinates=coordinates)

    @staticmethod
    def box_coordinates(s_cidx: int, s_ridx: int) -> Iterable[Tuple[int, int]]:
        for cidx in range(s_cidx, s_cidx + 3):
            for ridx in range(s_ridx, s_ridx + 3):
                yield cidx, ridx


class SudokuBoard:
    """Class representing a Sudoku Playboard with fields and collections."""
    fields: List[Field]
    collections: List[Collection]

    def __init__(self, sudoko_string: str):
        self._create_collections()
        self.fields = list()

        for ridx, row in enumerate(sudoko_string.strip().split("\n")):
            for cidx, cell in enumerate(row.split()):
                coordinate = (ridx, cidx)
                coll = list(self.collections_at_coo(coordinate))

                play_field = Field(
                    coordinate,
                    collections=coll,
                    value=self.str_to_value(cell)
                )
                self.fields.append(play_field)

    def is_solved(self) -> bool:
        """Check whether all collections are completed."""
        for coll in self.collections:
            if not coll.is_complete():
                return False
        return True

    def verify(self) -> bool:
        """Verify whether a board is valid."""
        for coll in self.collections:
            seen = set()
            for coo in coll.coordinates:
                play_field = self.field_at_coo(coo)
                if play_field.value is None:
                    continue
                if play_field.value in seen:
                    return False
                seen.add(play_field.value)
            assert seen == coll.values, f"{seen} != {coll.values}"

        return True

    @staticmethod
    def str_to_value(value_char: str) -> Optional[int]:
        assert len(value_char) == 1, "Not a single character."
        assert value_char in list("_123456789")
        if value_char == "_":
            return None
        return int(value_char)

    @staticmethod
    def value_to_str(value: Optional[int]) -> str:
        if value is None:
            return "_"
        return str(value)

    @staticmethod
    def every_n(iterable: Iterable[str], insert: str, num: int):
        for idx, string in enumerate(iterable):
            yield string
            if not (idx+1) % num:
                yield insert

    def _create_collections(self):
        """Create all collections for the sudoko board."""
        self.collections = list()

        # make boxes
        for ridx in range(0, 9, 3):
            for cidx in range(0, 9, 3):
                self.collections.append(SudokuBox(cidx=cidx, ridx=ridx))
        # make columns
        for cidx in range(0, 9):
            self.collections.append(SudokuColumn(cidx=cidx))
        # make rows
        for ridx in range(0, 9):
            self.collections.append(SudokuRow(ridx=ridx))

    def field_at_coo(self, coordinate: Tuple[int, int]) -> Field:
        play_field = self.fields[coordinate[0] * 9 + coordinate[1]]
        assert play_field.coordinate == coordinate
        return play_field

    def collections_at_coo(
        self, coordinate: Tuple[int, int]
    ) -> Iterable[Collection]:
        """Yield all collections linked to given coordinate."""
        for collection in self.collections:
            if coordinate in collection:
                yield collection

    def iter_available(self) -> Iterable[Field]:
        """Return an iterable over all fields where value is None."""
        return filter(lambda pf: pf.value is None, self.__iter__())

    def __iter__(self) -> Iterable[Field]:
        return iter(sorted(self.fields))

    def __str__(self) -> str:
        vertical_row = "-" * 25
        row_list = [vertical_row]
        for ridx, row_fields in itertools.groupby(self.fields, key=lambda pf: pf.coordinate[0]):
            row_vals = map(lambda pf: pf.value, row_fields)
            row_chars = map(self.value_to_str, row_vals)
            row_with_delimiters = self.every_n(row_chars, "|", 3)
            row_with_start = itertools.chain(["|"], row_with_delimiters)
            row_list.append(" ".join(row_with_start))
            if ridx in (2, 5, 8):
                row_list.append(vertical_row)
        return "\n".join(row_list)

    def __repr__(self) -> str:
        row_list = []
        for _, row_fields in itertools.groupby(self.fields, key=lambda pf: pf.coordinate[0]):
            row_vals = map(lambda pf: pf.value, row_fields)
            row_chars = map(self.value_to_str, row_vals)
            row_list.append(" ".join(row_chars))
        return "\n".join(row_list)
